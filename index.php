<?php

require_once __DIR__.'/vendor/autoload.php';
use stringoperations\StringUtils;

$str_ops = new StringUtils();
echo $str_ops::leftPad('pad-left', 15, '.');
echo $str_ops::rightPad('pad-right', 15, '.');
echo $str_ops::reverseString('sample');
$array = array(1 => 'a', 2 => 'b', 3 => 'c', 4 => 'd');
echo $str_ops::arrayToJSON($array);
var_dump($str_ops::sringToArray('sample string'));
