<?php
namespace stringoperations;

class StringUtils
{
    
    public static function leftPad($string, $length, $pad_string = '.') 
    {
        return str_pad($string, $length, $pad_string, STR_PAD_LEFT);
    }
    
    public static function rightPad($string, $length, $pad_string = '.') 
    {
        return str_pad($string, $length, $pad_string);
    }
    
    public function reverseString($string)
    {
        return strrev($string);
    }
    
    public function sringToArray($string)
    {
        return explode(' ',$string);
    }
    
    public function arrayToJSON($array)
    {
        return serialize($array);
    }
}